<?php
require 'database.php';

$nameError = $descriptionError = $passwordError = $categoryError = $imageError = $name = $description = $password = $category = $img = "";

if(!empty($_POST))
{
    $name               = checkInput($_POST['name']);
    $password           = password_hash(checkInput($_POST['password']), PASSWORD_DEFAULT);
    $category           = 7;
    $isSuccess          = true;

    if(empty($name))
    {
        $nameError = 'Ce champ nom ne peut pas être vide';
        $isSuccess = false;
    }
    if(empty($password))
    {
        $passwordError = 'Ce champ mdp ne peut pas être vide';
        $isSuccess = false;
    }

    if($isSuccess)
    {
        $db = Database::connect();
        $statement = $db->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
        $statement->execute(array($name, $password));
        $statement = $db->prepare('SELECT id FROM users WHERE username = ?');
        $statement->execute(array($name));
        $userId = $statement->fetch();
        $path = "../users/".$userId["id"];
        $imagePath = "../users/".$userId["id"]."/images/";

        if (!mkdir($imagePath, 0777, true)) {
            die('Echec lors de la création des répertoires...');
        }
        chmod($imagePath, 0777);
        Database::disconnect();
        header("Location: ../index.php");
    }
}

function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
