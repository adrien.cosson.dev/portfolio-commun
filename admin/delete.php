<?php
//session_start();
//if(!isset($_SESSION["username"])) {
//    header("location:../index.php");
//}
require 'database.php';
if (!empty($_GET))
{
    $id = checkInput($_GET['id']);
//    $idP = checkInput($_GET['idP']);
//        $admin = checkInput($_GET['admin']);
    $resourceCategory = checkInput($_GET['resourceCategory']);

}


    if(!empty($_POST)) 
    {
        $id = checkInput($_POST['id']);
//        $idP = checkInput($_POST['idP']);
        $resourceCategory = checkInput($_POST['resourceCategory']);
        $db = Database::connect();
        if ($resourceCategory == 1) {
            $statement = $db->prepare("DELETE FROM projectsCorrections WHERE id = ?");
            $statement->execute(array($id));
        } else if ($resourceCategory == 2) {
            $statement = $db->prepare("DELETE FROM cours WHERE id = ?");
            $statement->execute(array($id));
        } else {
            $statement = $db->prepare("DELETE FROM projects WHERE id = ?");
            $statement->execute(array($id));
        }
        Database::disconnect();
        header("Location: index.php?id=$id");
    }

    function checkInput($data) 
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Burger Code</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/styles.css?v=1">
    </head>
    
    <body>
         <div class="container admin">
            <div class="row">
                <h1><strong>Supprimer un item</strong></h1>
                <br>
                <form class="form" action="delete.php" role="form" method="post">
                    <input type="hidden" name="id" value="<?php echo $id;?>"/>
<!--                    <input type="hidden" name="idP" value="--><?php //echo $idP;?><!--"/>-->
                    <input type="hidden" name="resourceCategory" value="<?php echo $resourceCategory;?>"/>
                    <p class="alert alert-warning">Etes vous sur de vouloir supprimer ?</p>
                    <div class="form-actions">
                      <button type="submit" class="btn btn-warning">Oui</button>
                      <a class="btn btn-default" href="index.php">Non</a>
                    </div>
                </form>
            </div>
        </div>   
    </body>
</html>

