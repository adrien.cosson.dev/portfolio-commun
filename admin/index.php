<?php
session_start();
require 'database.php';
//if (!empty($_GET['id'])) {
//    $id = checkInput($_GET['id']);
//}
//var_dump($_SESSION);
//$_SESSION["username"] = "cadiyak";
if(!isset($_SESSION["username"])) {
    header("location:../index.php");
}
//echo $_SESSION["username"];
$db = Database::connect();
$statement = $db->prepare('SELECT id FROM users WHERE username = ?');
$statement->execute(array($_SESSION["username"]));
$userId = $statement->fetch();

$db = Database::disconnect();
;function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adoptundev</title>
    <link rel="stylesheet" href="../css/adminIndex.css?v=2">
    <link rel="stylesheet" href="../css/modalLogin.css">
    <link rel="stylesheet" href="../css/navbar.css?v=1.3">
    <link rel="stylesheet" href="../css/footer.css?v=1.1">
    <script src="../js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

</head>
<body>
<header>
<div id="wrap">
    <div>
        <span class="close">
            <a href="#"> </a>
        </span>
        <h1><strong>adopt</strong>un<strong>dev.com</strong></h1>
        <a href="#wrap" id="open">
            <svg class="burger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
            </svg>
        </a>


        <?php
        $db = Database::connect();
        echo '<nav>
            <div class="tab">
                <ul>';

        $statement = $db->query('SELECT categories.id, projects.category AS project FROM categories JOIN projects ON categories.id = projects.category GROUP BY id');
        $categories = $statement->fetchAll();

        $projectPage = count($categories) + 1;
        $profilePage = count($categories) + 2;

        echo '<li><a class="tablinks" href="../index.php">Home</a></li>';
        echo '<li><a class="tablinks" href="#" onclick="openCategory(event, ' . $id . ')">Profil</a></li>';
        $statement = $db->prepare('SELECT projects.category, categories.name AS name FROM projects INNER JOIN categories ON projects.category = categories.id WHERE projects.user = ? GROUP BY category');
        $statement->execute(array($id));
        $categoryProjects = $statement->fetchAll();
        //        if (!empty($categoryProjects)) {
        //            echo '<li class="hiddenMenu"><a class="tablinks" href="#" onclick="openCategory(event, ' . $projectPage . ')">Projets</a>';
        //            echo '<ul class="underMenu">';
        //            foreach ($categoryProjects as $category) {
        //                echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $category['category'] . ')">' . $category['name'] . '</a></li>';
        //            }
        //            echo '</ul></li>';
        //        }
        if (isset($_SESSION["username"])) {
            echo '<li><a id="default" class="tablinks" onclick="openCategory(event, 10)">Articles</a></li>';
            echo '<li><a class="login" href="logout.php">Logout</a></li>';
        } else {
            echo '<li><a href="#" id="loginButton">Login</a></li>';
        }
        echo ' </ul>
                </div>
                    <a href="#" id="close">×</a>
            </nav>
        
    
</div>
</header>';
        $statement = $db->prepare('SELECT * FROM users WHERE users.id = ?');

        $statement->execute(array($_SESSION["id"]));
        $user = $statement->fetch();
        //print_r($user);
        echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $id);
        $n = rand(100, 1000);
        echo '<article>
    <div class="article-content">';
        if (!empty($user['img'])) {
//            echo '<img src="images/' . $user['img'] . '" alt="...">';
            echo '<img src="users/'. $userId['id'].'/images/'. $user['img'] . '" alt="...">';
        } else {
            echo '<img src="https://picsum.photos/id/' . $n . '/300/200" alt="">';
        }
        echo '<div>
            <h2>' . $user['username'] . '</h2>
            <p>' . $user['description'] . '</p>
            <div class="article-footer">
                <a href="admin/profile.php?id=' . $_SESSION["id"] . '" class="button">Consulter</a>
            </div>
        </div>
    </div>
</article>
</div>';
        ?>
        <!--        echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $id);-->
        <div class="article-list tabcontent" id="10">
            <div class="container">
                <div class="tab">
                    <button id="defaultItem" class="tablinksItem" onclick="openItems(event, 'articles')">Projets
                    </button>
                    <?php
                    $statement = $db->query('SELECT categoryName, id FROM resources');
                    $resources = $statement->fetchAll();
                    foreach ($resources as $resource) {
                        echo '<button class="tablinksItem" onclick="openItems(event, ' . $resource['id'] . ')">' . $resource['categoryName'] . '</button>';
                    }
                    ?>

                </div>
                <div id="articles" class="tabcontentItem">
                    <!--				<a class="btn btn-default btn-lg" href="../index.php">Retour au site   <span class="glyphicon glyphicon glyphicon-log-in"></span></a>-->
                    <h1><strong>Liste des articles </strong><a href="insert.php?id=<?php echo $_SESSION["id"] ?>" id="button"
                                                               class="button">Ajouter</a></h1>
                    <table>
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th class="hidden" style="border: none;">Description</th>
                            <!--                      <th class="hidden">Adresse</th>-->
                            <th>Catégorie</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $db = Database::connect();
                        //                      $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, projects.id, categories.name AS category FROM projects right JOIN categories ON projects.category = categories.id  WHERE projects.user = ?');
                        //                      $statement = $db->prepare('SELECT * FROM projectsCorrections UNION SELECT * FROM projects WHERE user= ?');
                        $statement = $db->prepare('SELECT projects.name, projects.description, projects.id, projects.admin, categories.name as category FROM projects JOIN categories ON projects.category = categories.id WHERE user= ?');
                        //                      $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, categories.name AS category FROM projects JOIN categories ON projects.category = categories.id WHERE user = ? ');
//                        $statement->execute(array($userId["id"]));
                        $statement->execute(array($_SESSION["id"]));

                        while ($projects = $statement->fetch()) {
                            echo '<tr>';
                            echo '<td>' . $projects['name'] . '</td>';
                            echo '<td class="hidden" style="border-bottom: none;">' . $projects['description'] . '</td>';
//                            echo '<td class="hidden">'. $projects['address'] . '</td>';
                            echo '<td>' . $projects['category'] . '</td>';
                            echo '<td style="width: 400px;">';
//                            var_dump($userId);
                            echo '<a class="button" href="view.php?idP=' . $projects['id'] . '&id=' . $userId['id'] . '"><svg aria-hidden="true"
                           focusable="false" data-prefix="fas" data-icon="eye"
                           class="svg-inline--fa fa-eye fa-w-18" role="img"
                           xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path
                            fill="currentColor"
                            d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288
                            448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 
                            144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspVoir</p></a>';


                            echo ' ';
                            echo '<a class="button" href="update.php?idP=' . $projects['id'] . '&id=' . $userId['id'] . '"><svg aria-hidden="true" focusable="false" data-prefix="fas"
 data-icon="pencil-alt" class="svg-inline--fa fa-pencil-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path 
 fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 
 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 
 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 
 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspModifier</p></a>';
                            echo ' ';
                            echo '<a class="button" href="delete.php?id=' . $projects['id'] . '"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspSupprimer</p></a>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        Database::disconnect();
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
                foreach ($resources as $resource) {
                echo sprintf("<div class=\"tabcontentItem\" id=\"%s\">", $resource['id']);
                $db = Database::connect();
                if ($resource["id"] == 1) {
//                $statement = $db->query('SELECT *, resources.id AS categoryR FROM projectsCorrections INNER JOIN resources ON resources.id = projectsCorrections.categoryResource');
//                    $statement = $db->query('SELECT * FROM projectsCorrections');
                    $statement = $db->prepare('SELECT projectsCorrections.id, projectsCorrections.name, projectsCorrections.description, categories.name AS categoryName FROM projectsCorrections LEFT JOIN categories ON projectsCorrections.category = categories.id WHERE user = ?');
//                    $statement->execute(array($userId["id"]));
                    $statement->execute(array($_SESSION["id"]));
                } else if ($resource["id"] == 2) {
//                $statement = $db->query('SELECT *, resources.id AS categoryR FROM cours INNER JOIN resources ON resources.id = cours.categoryResource');
                    $statement = $db->prepare('SELECT cours.id, cours.name, cours.description, categories.name AS categoryName FROM cours LEFT JOIN categories ON cours.category = categories.id WHERE user = ?');
//                    $statement->execute(array($userId["id"]));
                    $statement->execute(array($_SESSION["id"]));

                }

                ?>
                <h1><strong>Liste des articles </strong><a
                            href="insert.php?id=<?php echo $_SESSION["id"] . "&resourceId=" . $resource["id"] ?>" id="button"
                            class="button">Ajouter</a></h1>
                <table>
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th class="hidden" style="border: none;">Description</th>
                        <!--                      <th class="hidden">Adresse</th>-->
                        <th>Catégorie</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    //                $db = Database::connect();
                    //                //                      $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, projects.id, categories.name AS category FROM projects right JOIN categories ON projects.category = categories.id  WHERE projects.user = ?');
                    //                //                      $statement = $db->prepare('SELECT * FROM projectsCorrections UNION SELECT * FROM projects WHERE user= ?');
                    //                $statement = $db->prepare('SELECT projectsCorrections.name, projectsCorrections.description, projectsCorrections.id, projectsCorrections.admin, resources.name as category FROM projectsCorrections JOIN resources ON projectsCorrections.categoryResource = resources.id WHERE user = ? AND projectsCorrections.categoryResource = ?');
                    //                //                      $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, categories.name AS category FROM projects JOIN categories ON projects.category = categories.id WHERE user = ? ');
                    //                $statement->execute(array($id, $resource["id"]));

                    while ($resources = $statement->fetch()) {
                        echo '<tr>';
                        echo '<td>' . $resources['name'] . '</td>';
                        echo '<td class="hidden" style="border-bottom: none;">' . $resources['description'] . '</td>';
//                            echo '<td class="hidden">'. $projects['address'] . '</td>';
                        echo '<td>' . $resources['categoryName'] . '</td>';
                        echo '<td style="width: 400px;">';
                        echo '<a class="button" href="view.php?idP=' . $resources['id'] . '&resourceCategory=' . $resource["id"] . '&id=' . $userId['id'] . '"><svg aria-hidden="true"
                           focusable="false" data-prefix="fas" data-icon="eye"
                           class="svg-inline--fa fa-eye fa-w-18" role="img"
                           xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path
                            fill="currentColor"
                            d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288
                            448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 
                            144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspVoir</p></a>';


                        echo ' ';
                        echo '<a class="button" href="update.php?idP=' . $resources['id'] . '&resourceCategory=' . $resource["id"] . '&id=' . $userId['id'] . '"><svg aria-hidden="true" focusable="false" data-prefix="fas"
 data-icon="pencil-alt" class="svg-inline--fa fa-pencil-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path 
 fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 
 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 
 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 
 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspModifier</p></a>';
                        echo ' ';
                        echo '<a class="button" href="delete.php?id=' . $resources['id'] . '&resourceCategory=' . $resource["id"] . '"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path>
                            </svg><p class="hiddenbutton">&nbsp&nbspSupprimer</p></a>';
                        echo '</td>';
                        echo '</tr>';
                    }
                    Database::disconnect();
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
        <footer>
            <a href="https://adriencosson.fr" target="_blank">2020 Adrien Cosson</a>
        </footer>
</body>
</html>
<script>
    document.getElementById("default").click();
    document.getElementById("defaultItem").click();

    function openItems(evt, ItemName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontentItem");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinksItem");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(ItemName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
