<?php

    require 'database.php';

    if(!empty($_POST["username"]))
    {
        $isSuccess     	   	= true;
        $verify             = false;
        if(empty(checkInput($_POST['username'])))
        {
            $isSuccess = false;
        }
        if(empty(checkInput($_POST['password'])))
        {
          	$isSuccess = false;
        }
        if($isSuccess)
        {
           		$db = Database::connect();
				$query = "SELECT id, username, password FROM users WHERE username = :username";
                $statement = $db->prepare($query);
                $statement->execute(
                     array(
                          'username'     =>     $_POST["username"],
                     )
                );
                $users = $statement->fetchAll();
                foreach ($users as $user) {
                    if (password_verify($_POST["password"], $user['password']))
                    {
                       $verify = true;
                        session_start();
                        $_SESSION["username"] = $_POST["username"];
                        $_SESSION["id"] = $user["id"];
                        echo json_encode(array('success' => 1));
                    }
                }
                if($verify != true)
                {
                        echo json_encode(array('success' => 0));
                }
			}
			Database::disconnect();
	}

    function checkInput($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }