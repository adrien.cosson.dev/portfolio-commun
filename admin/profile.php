<?php
session_start();
require 'database.php';
if (!empty($_GET['id'])) {
    $id = checkInput($_GET['id']);
    $db = Database::connect();
    $statement = $db->prepare('SELECT username, id FROM users WHERE users.id = ?');
    $statement->execute(array($id));
    $user = $statement->fetch();

}
function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adoptundev</title>
    <link rel="stylesheet" href="../css/profile.css?v=1.6">
    <link rel="stylesheet" href="../css/modalLogin.css">
    <link rel="stylesheet" href="../css/navbar.css?v=1.2">
    <link rel="stylesheet" href="../css/footer.css">
    <script src="../js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <span class="close">
            <a href="#"> </a>
        </span>
        <h1><strong>adopt</strong>un<strong>dev.com</strong></h1>
        <a href="#wrap" id="open">
            <svg class="burger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
            </svg>
        </a>


        <?php
        $db = Database::connect();
        echo '<nav>
            <div class="tab">
                <ul>';

        $statement = $db->query('SELECT categories.id, projects.category AS project FROM categories JOIN projects ON categories.id = projects.category GROUP BY id');
        $categories = $statement->fetchAll();

        $projectPage = count($categories) + 1;
        $profilePage = 21;
        //        var_dump(count($categories));

        echo '<li><a class="tablinks" href="../index.php">Home</a></li>';
        echo '<li><a class="tablinks" href="#" id="default" onclick="openCategory(event, ' . $profilePage . ')">Profil</a></li>';
        $statement = $db->prepare('SELECT projects.category, categories.name AS name FROM projects INNER JOIN categories ON projects.category = categories.id WHERE projects.user = ? GROUP BY category');
        $statement->execute(array($id));
        $categoryProjects = $statement->fetchAll();
        if (!empty($categoryProjects)) {
            echo '<li class="hiddenMenu"><a class="tablinks" href="#" onclick="openCategory(event, 0)">Projets</a>';
            echo '<ul class="underMenu">';
            foreach ($categoryProjects as $category) {
                echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $category['category'] . ')">' . $category['name'] . '</a></li>';
            }
            echo '</ul></li>';
        }
        if (isset($_SESSION["username"])) {
            echo '<li><a href="index.php?id=' . $id . '">' . $_SESSION["username"] . '</a></li>';
            echo '<li><a class="login" href="logout.php">Logout</a></li>';
        } else {
            echo '<li><a href="#" id="loginButton">Login</a></li>';
        }
        echo ' </ul>
                </div>
                    <a href="#" id="close">×</a>
            </nav>';
        ?>
    </header>
</div>


<main class="container">

    <?php
    foreach ($categories as $category) {
        echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $category['id']);
        $statement = $db->prepare('SELECT * FROM projects WHERE projects.category = ? AND projects.user = ?');
        $statement->execute(array($category["id"], $id));
        while ($project = $statement->fetch()) {
            echo '<article>
                    <div class="article-content">';
//var_dump($user);
            if (!empty($project['img'])) {
                echo '<img src="../users/'.$user["id"].'/images/' . $project['img'] . '" alt="...">';
            } else {
                echo '<img src="https://picsum.photos/300/200" alt="">';

            }
            echo '<div>
                                <h2>' . $project['name'] . '</h2>
                                    <p>' . $project['description'] . '</p>
                                        <div class="article-footer">';
            if (!empty($project['address'])) {
                echo '<a href="' . $project['address'] . '" class="button" target="_blank">Consulter</a>';
            } else {
                echo '<a class="button grey" target="_blank">Consulter</a>';
            }
            echo '</div>
                            </div>
                    </div>
                </article>';
        }
        echo '</div>';
    }
    ?>
    <div class="article-list tabcontent" id="0")>
    <?php
    $statement = $db->prepare('SELECT * FROM projects WHERE projects.user = ?');
    $statement->execute(array($id));
    while ($project = $statement->fetch()) {
        echo '<article>
                    <div class="article-content">';

        if (!empty($project['img'])) {
            echo '<img src="../users/'.$user["id"].'/images/' . $project['img'] . '" alt="...">';
        } else {
            echo '<img src="https://picsum.photos/300/200" alt="">';

        }
        echo '<div> 
                                <h2>' . $project['name'] . '</h2>
                                    <p>' . $project['description'] . '</p>
                                        <div class="article-footer">';
        if (!empty($project['address'])) {
            echo '<a href="' . $project['address'] . '" class="button" target="_blank">Consulter</a>';
        } else {
            echo '<a class="button grey" target="_blank">Consulter</a>';
        }
        echo '</div> 
                            </div>
                    </div>
                </article>';
    }
    echo '</div>';


    $statement = $db->prepare('SELECT * FROM users WHERE users.id = ?');
    $statement->execute(array($id));
    $user = $statement->fetch();
    echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $profilePage);

    echo '<article>
                    <div class="article-content">';

    if (!empty($user['img'])) {
        echo '<img src="../images/' . $user['img'] . '" alt="...">';
    } else {
        echo '<img src="https://picsum.photos/300/200" alt="">';

    }
    echo '<div> 
                                <h2>' . $user['username'] . '</h2>
                                    <p>' . $user['description'] . '</p>
                                        <div class="article-footer">';
    if (!empty($user['address'])) {
        echo '<a href="' . $user['address'] . '" class="button" target="_blank">Consulter</a>';
    } else {
        echo '<a class="button grey" target="_blank">Consulter</a>';
    }
    echo '</div> 
                            </div>
                    </div>
                </article>
                </div>';

    Database::disconnect();
    ?>

</main>
<div id="loginModal" class="modal">
    <div class="modal-content">
        <!--        <span class="closeModal">&times;</span>-->
        <input type="hidden" class="closeModal">
        <div class="modal-header">
            <div class="tabmodal">
                <a class="tablinksmodal" id="defaultModal" onclick="openModal(event, 'login')">Login</a><a
                        class="tablinksmodal" onclick="openModal(event, 'inscription')">Inscription</a>
            </div>
        </div>

        <div class="modal-body">
            <div id="login" class="tabcontentmodal">
                <!--                    <h3>London</h3>-->
                <form id="form" class="form" method="post">
                    <!--                    <p>Nom d'utilisateur</p>-->
                    <label for="username">Nom d'utilisateur</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <span class="eye"><svg display="none" id="eye" onclick="showPassword()" aria-hidden="true"
                                               focusable="false" data-prefix="fas" data-icon="eye"
                                               class="svg-inline--fa fa-eye fa-w-18" role="img"
                                               xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path
                                        fill="currentColor"
                                        d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path></svg>
                            <svg display="none" id="closedEye" onclick="showPassword()" aria-hidden="true"
                                 focusable="false" data-prefix="fas" data-icon="eye-slash"
                                 class="svg-inline--fa fa-eye-slash fa-w-20" role="img"
                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor"
                                                                                                d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"></path></svg>
                            </span>
                        <p id="passwordLoginError">Identifiants incorrects</p>
                    </div>
                    <br>
                    <div class="form-actions">
                        <button class="loginButton" type="submit" name="login">Login</button>
                    </div>

                </form>
            </div>
            <div id="inscription" class="tabcontentmodal">
                <form class="form" action="adduser.php" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Nom d'utilisateur</label>
                        <input type="text" class="form-control" id="name" name="name">
                        <p class="help-inline"><?php echo $nameError; ?></p>
                    </div>
                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" id="passwordInscription" name="password">
                        <span class="eye"><svg display="none" id="eyeIns" onclick="showPasswordInscription()"
                                               aria-hidden="true" focusable="false" data-prefix="fas" data-icon="eye"
                                               class="svg-inline--fa fa-eye fa-w-18" role="img"
                                               xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path
                                        fill="currentColor"
                                        d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path></svg>
                            <svg display="none" id="closedEyeIns" onclick="showPasswordInscription()" aria-hidden="true"
                                 focusable="false" data-prefix="fas" data-icon="eye-slash"
                                 class="svg-inline--fa fa-eye-slash fa-w-20" role="img"
                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor"
                                                                                                d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"></path></svg>
                            </span>
                        <p class="help-inline"><?php echo $passwordError; ?></p>
                    </div>
                    <br>
                    <div class="form-actions">
                        <button type="submit" class="loginButton">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
    <a href="https://adriencosson.fr" target="_blank">2020 Adrien Cosson</a>
</footer>
</body>
</html>
<script>
    // click on default tab
    document.getElementById("default").click();
    document.getElementById("defaultModal").click();
    var passwordInscription = document.getElementById("passwordInscription");
    var password = document.getElementById("password");
    var eye = document.getElementById("eye");
    var eyeIns = document.getElementById("eyeIns");
    var closedEye = document.getElementById("closedEye");
    var closedEyeIns = document.getElementById("closedEyeIns");
    // Get the modal
    var modal = document.getElementById("loginModal");

    // Get the button that opens the modal
    var btn = document.getElementById("loginButton");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("closeModal")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
        // document.getElementById("defaultModal").click();
        // tabcontent[7].style.display = "block";

    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    function showPasswordInscription() {
        if (passwordInscription.type === "password") {
            passwordInscription.type = "text";
            closedEyeIns.style.display = "block"
            eyeIns.style.display = "none"
        } else {
            passwordInscription.type = "password";
            eyeIns.style.display = "block"
            closedEyeIns.style.display = "none"
        }
    }

    function showPassword() {
        if (password.type === "password") {
            password.type = "text";
            closedEye.style.display = "block"
            eye.style.display = "none"
        } else {
            password.type = "password";
            eye.style.display = "block"
            closedEye.style.display = "none"
        }
    }


    $(document).ready(function () {
        $('#form').submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: 'login.php',
                data: $(this).serialize()
            }).then(
                function (response) {
                    var jsonData = JSON.parse(response);

                    if (jsonData.success === 1) {
                        location.href = 'index.php';
                    } else {
                        // alert('Mauvais identifiants!');
                        passwordLoginError = document.getElementById("passwordLoginError");
                        passwordLoginError.style.display = "inline-block";
                    }
                },

                function () {
                    alert('I y à eu un problème!');
                }
            );
        });
    });
</script>