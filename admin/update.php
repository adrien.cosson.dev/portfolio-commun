<?php
//session_start();
//if(!isset($_SESSION["username"])) {
//    header("location:../index.php");
//}
    require 'database.php';
    if (!empty($_GET))
    {
        $id = checkInput($_GET['id']);
        $idP = checkInput($_GET['idP']);
//        $admin = checkInput($_GET['admin']);
        $resourceCategory = checkInput($_GET['resourceCategory']);

    }

    $nameError = $descriptionError = $addressError = $gitAddressError = $categoryError = $imageError = $name = $description = $price = $address = $gitAddress = $category = $image = "";

    if(!empty($_POST)) 
    {
        $id = checkInput($_POST['id']);
        $idP = checkInput($_POST['idP']);
//        $admin = checkInput($_POST['admin']);
        $resourceCategory = checkInput($_POST['resourceCategory']);

        $name               = checkInput($_POST['name']);
        $description        = checkInput($_POST['description']);
        $address            = checkInput($_POST['address']);
        $gitAddress         = checkInput($_POST['gitAddress']);
        $category           = checkInput($_POST['category']);
        $image              = checkInput($_FILES["image"]["name"]);
//        $imagePath          = '../images/'. basename($image);
//        $imageExtension     = pathinfo($imagePath,PATHINFO_EXTENSION);
        $isSuccess          = true;

//        $db = Database::connect();
//        $statement = $db->prepare('SELECT users.username FROM users WHERE users.id = ?');
//        $statement->execute(array($id));
//        $username = $statement->fetch();
//        var_dump($username['username']);
//        $path = '../users/'.$username['username'].'/images/';
//        $imagePath = $path. basename($image);
//        $imageExtension     = pathinfo($imagePath,PATHINFO_EXTENSION);



        if ($resourceCategory == 1)  {
//           $path = '../users/'.$username['username'].'/corrections/images/';
            $path = '../users/'.$id.'/corrections/images/';
        } else if ($resourceCategory == 2) {
//           $path = '../users/' . $username['username'] . '/cours/images/';
            $path = '../users/' . $id . '/cours/images/';
        } else {
            $path = '../users/'.$id.'/images/';
        }
        if (!file_exists($path))  {
            if (!mkdir($path, 0777, true)) {
                die('Echec lors de la création des répertoires...');
            }

        }
        $imagePath = $path. basename($image);
        $imageExtension     = pathinfo($imagePath,PATHINFO_EXTENSION);







        if(empty($name)) 
        {
            $nameError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if(empty($description)) 
        {
            $descriptionError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if(empty($address))
        {
            $addressError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if(empty($gitAddress))
        {
            $gitAddressError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if(empty($category))
        {
            $categoryError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if(empty($image)) // le input file est vide, ce qui signifie que l'image n'a pas ete update
        {
            $isImageUpdated = false;
        } else
        {
            $isImageUpdated = true;
            $isUploadSuccess =true;
            if($imageExtension != "jpg" && $imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "gif" )
            {
                $imageError = "Les fichiers autorises sont: .jpg, .jpeg, .png, .gif";
                $isUploadSuccess = false;
            }
            if(file_exists($imagePath))
            {
                $imageError = "Le fichier existe deja";
                $isUploadSuccess = false;
            }
            if($_FILES["image"]["size"] > 500000)
            {
                $imageError = "Le fichier ne doit pas depasser les 500KB";
                $isUploadSuccess = false;
            }
            if($isUploadSuccess)
            {
                if(!move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath))
                {
                    $imageError = "Il y a eu une erreur lors de l'upload";
                    $isUploadSuccess = false;
                }
            }
        }
         

        if (($isSuccess && $isImageUpdated && $isUploadSuccess) || ($isSuccess && !$isImageUpdated)) {
            $db = Database::connect();
            if ($resourceCategory == 1) {
                $statement = $db->prepare('UPDATE projectsCorrections set name = ?, description = ?, address = ?, gitAddress = ?, img = ? WHERE projectsCorrections.id = ?');
//                $statement = $db->prepare("UPDATE projectCorrections set name = ?, description = ?, address = ?, gitAddress = ?, img = ? WHERE id = ?");
                $statement->execute(array($name,$description,$address,$gitAddress,$image,$idP));
                $projects = $statement->fetch();
            } else if ($resourceCategory == 2) {
                $statement = $db->prepare('UPDATE cours set name = ?, description = ?, address = ?, img = ? WHERE cours.id = ?');
                $statement->execute(array($name,$description,$address,$image,$idP));
                $projects = $statement->fetch();
            } else  {
                $statement = $db->prepare("UPDATE projects set name = ?, description = ?, address = ?, gitAddress = ?, img = ? WHERE id = ?");
                $statement->execute(array($name,$description,$address,$gitAddress,$image,$idP));
                $projects = $statement->fetch();
            }

//            if ($admin == 0) {
//                $statement = $db->prepare("UPDATE projects set name = ?, description = ?, address = ?, gitAddress = ?, img = ? WHERE id = ?");
//                $statement->execute(array($name,$description,$address,$gitAddress,$image,$idP));
//                $projects = $statement->fetch();
//            } if ($admin == 1) {
//            $statement = $db->prepare('SELECT * FROM projectsCorrections WHERE projectsCorrections.id = ?');
//                $statement = $db->prepare("UPDATE projectCorrections set name = ?, description = ?, address = ?, gitAddress = ?, img = ? WHERE id = ?");
//                $statement->execute(array($name,$description,$address,$gitAddress,$image,$idP));
//            }
}
            Database::disconnect();
    }
        $db = Database::connect();
//        if ($admin == 1) {
//            $statement = $db->prepare('SELECT projectsCorrections.name, projectsCorrections.description, projectsCorrections.address, projectsCorrections.gitAddress, projectsCorrections.img, projectsCorrections.category FROM projectsCorrections WHERE projectsCorrections.id = ?');
//            $statement->execute(array($idP));
//            $projects = $statement->fetch();
//        }
//        if ($admin == 0) {
//            $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, projects.gitAddress, projects.img, projects.category FROM projects WHERE projects.id = ?');
//            $statement->execute(array($idP));
//            $projects = $statement->fetch();
//        }
if ($resourceCategory == 1) {
    $statement = $db->prepare('SELECT *, resources.id AS categoryR FROM projectsCorrections INNER JOIN resources ON resources.id = projectsCorrections.resourceCategory WHERE projectsCorrections.id = ?');
    $statement->execute(array($idP));
    $projects = $statement->fetch();
} else if ($resourceCategory == 2) {
    $statement = $db->prepare('SELECT *, resources.id AS categoryR FROM cours INNER JOIN resources ON resources.id = cours.resourceCategory WHERE cours.id = ?');
    $statement->execute(array($idP));
    $projects = $statement->fetch();
} else  {
    $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, projects.gitAddress, projects.img, categories.name AS category FROM projects JOIN categories ON projects.category = categories.id WHERE projects.id = ?');
    $statement->execute(array($idP));
    $projects = $statement->fetch();
}
        $currentName               = $projects["name"];
        $currentDescription        = $projects["description"];
        $currentAddress            = $projects["address"];
        $currentGitAddress         = $projects["gitAddress"];
        $currentCategory           = $projects["category"];
        $currentImage              = $projects["img"];
        Database::disconnect();

    function checkInput($data) 
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

?>



<!DOCTYPE html>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adoptundev</title>
    <link rel="stylesheet" href="../css/view.css?v=1.3">
    <link rel="stylesheet" href="../css/navbar.css">
    <script src="../js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <span class="close">
            <a href="#"> </a>
        </span>
        <h1><strong>adopt</strong>un<strong>dev.com</strong></h1>
        <a href="#wrap" id="open">
            <svg class="burger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
            </svg>
        </a>


        <nav>
            <div class="tab">
                <ul>

        <li><a class="tablinks" href="../index.php">Home</a></li>
        <li><a class="tablinks" href="#" id="default" onclick="openCategory(event, 1)">Profil</a></li>
<!--        //        $statement = $db->prepare('SELECT projects.category, categories.name AS name FROM projects-->
<!--        //        INNER JOIN categories ON projects.category = categories.id WHERE projects.user = ?-->
<!--        //        GROUP BY category');-->
<!--        //        $statement->execute(array($id));-->
<!--        //        $categoryProjects = $statement->fetchAll();-->
<!--        //        if (!empty($categoryProjects)) {-->
<!--        //            echo '<li class="hiddenMenu"><a class="tablinks" href="#" onclick="openCategory(event, ' . $projectPage . ')">Projets</a>';-->
<!--        //            echo '<ul class="underMenu">';-->
<!--        //            foreach ($categoryProjects as $category) {-->
<!--        //                echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $category['category'] . ')">' . $category['name'] . '</a></li>';-->
<!--        //            }-->
<!--        //            echo '</ul></li>';-->
<!--        //        }-->
        <?php
        if (isset($_SESSION["username"])) {
            echo '<li><a href="index.php?id=' . $id . '">Articles</a></li>';
            echo '<li><a class="login" href="logout.php">Logout</a></li>';
        } else {
            echo '<li><a href="#" id="loginButton">Login</a></li>';
        }
        ?>
        </ul>
                </div>
                    <a href="#" id="close">×</a>
            </nav>
    </header>
</div>

    <body>
         <div class="container admin">
            <div class="row">
                <div class="col-md-6">
                    <h1><strong>Modifier un item</strong></h1>
                    <br>
                    <form class="form" action="<?php echo 'update.php?id='.$id;?>" role="form" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="resourceCategory" name="resourceCategory" value="<?php echo $resourceCategory; ?>">
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
                        <input type="hidden" id="idP" name="idP" value="<?php echo $idP; ?>">
                        <div class="form-group">
							<label for="name">Nom:</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="name" value="<?php echo $currentName; ?>">
                            <span class="help-inline"><?php echo $nameError;?></span>
                        </div>
                        <div class="form-group">
							<label for="description">Description:</label>
                            <input type="textarea" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $currentDescription; ?>">
                            <span class="help-inline"><?php echo $descriptionError;?></span>
                        </div>
                        <div class="form-group">
							<label for="address">Adresse :</label>
                            <input type="text" step="0.01" class="form-control" id="address" name="address" placeholder="Adresse" value="<?php echo $currentAddress; ?>">
                            <span class="help-inline"><?php echo $addressError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="gitAddress">Adresse gitlab :</label>
                            <input type="text" step="0.01" class="form-control" id="gitAddress" name="gitAddress" placeholder="Adresse gitlab" value="<?php echo $currentGitAddress; ?>">
                            <span class="help-inline"><?php echo $gitAddressError;?></span>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="category">Catégorie :</label>-->
<!--                            <input type="text" step="0.01" class="form-control" id="category" name="category" placeholder="Catégorie" value="--><?php //echo $currentCategory; ?><!--">-->
<!--                            <span class="help-inline">--><?php //echo $categoryError;?><!--</span>-->
<!--                        </div>-->

                        <div class="form-group">
							<label for="category">Catégorie:</label>
                            <select class="form-control" id="category" name="category">
                            <?php
                               $db = Database::connect();
                               foreach ($db->query('SELECT * FROM categories') as $row)
                               {
                                    if($row['id'] == $category)
                                        echo '<option selected="selected" value="'. $row['id'] .'">'. $row['name'] . '</option>';
                                    else
                                        echo '<option value="'. $row['id'] .'">'. $row['name'] . '</option>';
                               }
                               Database::disconnect();
                            ?>
                            </select>
                            <span class="help-inline"><?php echo $categoryError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="image">Image:</label>
                            <p><?php echo $projects['img'];?></p>
                            <label for="image">Sélectionner une nouvelle image:</label>
                            <input type="file" id="image" name="image"> 
                            <span class="help-inline"><?php echo $imageError;?></span>
                        </div>
                        <br>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Modifier</button>
                            <a href="index.php?id=<?php echo $id ?>">Retour</a>
                       </div>
                    </form>
                </div>
<!--                <div class="col-sm-6 site">-->
<!--                    <div class="thumbnail">-->
<!--                        <img src="--><?php //echo '../images/'.$image;?><!--" alt="...">-->
<!--                        <div class="price">--><?php //echo number_format((float)$price, 2, '.', ''). ' €';?><!--</div>-->
<!--                          <div class="caption">-->
<!--                            <h4>--><?php //echo $name;?><!--</h4>-->
<!--                            <p>--><?php //echo $description;?><!--</p>-->
<!--                            <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>-->
<!--                          </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>   
    </body>
</html>
