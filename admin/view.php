<?php
session_start();
if(!isset($_SESSION["username"])) {
    header("location:../index.php");
}
require 'database.php';
if (!empty($_GET['id'])) {
    $id = checkInput($_GET['id']);
    $idP = checkInput($_GET['idP']);
    $resourceCategory = checkInput($_GET['resourceCategory']);
}
function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adoptundev</title>
    <link rel="stylesheet" href="../css/view.css?v=1.3">
    <link rel="stylesheet" href="../css/navbar.css">
    <script src="../js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <span class="close">
            <a href="#"> </a>
        </span>
        <h1><strong>adopt</strong>un<strong>dev.com</strong></h1>
        <a href="#wrap" id="open">
            <svg class="burger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
            </svg>
        </a>


        <?php

        echo '<nav>
            <div class="tab">
                <ul>';

        $db = Database::connect();
//echo $resourceCategory;
//echo $idP;
        if ($resourceCategory == 1) {
            $statement = $db->prepare('SELECT * FROM projectsCorrections WHERE projectsCorrections.id = ?');
            $statement->execute(array($idP));
            $projects = $statement->fetch();
        } else if ($resourceCategory == 2) {
            $statement = $db->prepare('SELECT * FROM cours WHERE cours.id = ?');
            $statement->execute(array($idP));
            $projects = $statement->fetch();
        } else  {
            $statement = $db->prepare('SELECT * FROM projects WHERE projects.id = ?');
            $statement->execute(array($idP));
            $projects = $statement->fetch();
        }

//        if ($admin == 0) {
////            $statement = $db->prepare('SELECT * FROM projects WHERE projects.id = ?');
//            $statement = $db->prepare('SELECT projects.name, projects.description, projects.address, projects.gitAddress, projects.img, categories.name AS category FROM projects JOIN categories ON projects.category = categories.id WHERE projects.id = ?');
//            $statement->execute(array($idP));
//            $projects = $statement->fetch();
//        }
//        if ($admin == 1) {
////            $statement = $db->prepare('SELECT * FROM projectsCorrections WHERE projectsCorrections.id = ?');
//            $statement = $db->prepare('SELECT projectsCorrections.name, projectsCorrections.description, projectsCorrections.address, projectsCorrections.gitAddress, projectsCorrections.img, categories.name AS category FROM projectsCorrections JOIN categories ON projectsCorrections.category = categories.id WHERE projectsCorrections.id = ?');
//            $statement->execute(array($idP));
//            $projects = $statement->fetch();
//        }
//        $statement = $db->query('SELECT * FROM projects WHERE projects.id = 1');
//        $projects = $statement->fetch();

        echo '<li><a class="tablinks" href="../index.php">Home</a></li>';
        echo '<li><a class="tablinks" href="#" id="default" onclick="openCategory(event, ' . $profilePage . ')">Profil</a></li>';
//        $statement = $db->prepare('SELECT projects.category, categories.name AS name FROM projects
//        INNER JOIN categories ON projects.category = categories.id WHERE projects.user = ?
//        GROUP BY category');
//        $statement->execute(array($id));
//        $categoryProjects = $statement->fetchAll();
//        if (!empty($categoryProjects)) {
//            echo '<li class="hiddenMenu"><a class="tablinks" href="#" onclick="openCategory(event, ' . $projectPage . ')">Projets</a>';
//            echo '<ul class="underMenu">';
//            foreach ($categoryProjects as $category) {
//                echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $category['category'] . ')">' . $category['name'] . '</a></li>';
//            }
//            echo '</ul></li>';
//        }
        if (isset($_SESSION["username"])) {
            echo '<li><a href="index.php?id=' . $id . '">Articles</a></li>';
            echo '<li><a class="login" href="logout.php">Logout</a></li>';
        } else {
            echo '<li><a href="#" id="loginButton">Login</a></li>';
        }
        echo ' </ul>
                </div>
                    <a href="#" id="close">×</a>
            </nav>';
        ?>
    </header>
</div>

<div class="container">
    <div class="half">
    <div>
        <div>
            <h1><strong>Voir un item</strong></h1>
            <br>
            <form>
                <div class="form-group">
                    <label>Nom:</label><?php echo '  ' . $projects['name'];?>
                </div>
                <div class="form-group">
                    <label>Description:</label><?php echo '  ' . $projects['description'];?>
                </div>
                <div class="form-group">
                    <label>Adresse:</label><?php echo '<a href="' . $projects['address'] . '">' . $projects['address'] .'</a>';?>
                </div>
                <div class="form-group">
                    <label>Adresse gitlab:</label>
                    <?php if (!empty($projects['gitAddress'])) {
                         echo '<a href="' . $projects['gitAddress'] . '">' . $projects['gitAddress'] .'</a>';
                    } else {
                        echo 'Aucune adresse git';
                    }?>

                </div>
                <div class="form-group">
                    <label>Catégorie:</label><?php echo '  ' . $projects['category'];?>
                </div>
                <div class="form-group">
                    <label>Image:</label>
                    <?php if (!empty($projects['img'])) {
                        echo '  ' . $projects['img'];
                    } else {
                        echo 'Aucune image';
                    }?>
                    
                </div>
            </form>
            <br>
            <div class="form-actions">
                <a class="button1" href="index.php?id=<?php echo $id?>"> Retour</a>
            </div>
        </div>
    </div>
</div>
<div class="half">
        <article>
            <div class="article-content">

                <?php
                if (!empty($projects['img'])) {
                    if ($resourceCategory == 1) {
                        echo '<img src="../users/'.$id.'/corrections/images/'. $projects['img'] . '" alt="...">';
                    } else if ($resourceCategory == 2) {
                        echo '<img src="../users/'.$id.'/cours/images/'. $projects['img'] . '" alt="...">';
                    } else {
                        echo '<img src="../users/'. $id.'/images/'. $projects['img'] . '" alt="...">';
                    }
//                echo '<img src="../images/' .  $projects['img'] . '" alt="...">';

                } else {
                echo '<img src="https://picsum.photos/300/200" alt="">';

                }?>
                <div>
                    <h2><?php echo $projects['name'] ?></h2>
                    <p><?php echo $projects['description'] ?></p>
                    <div class="article-footer">
                        <?php if (!empty($projects['address'])) {
                        echo '<a href="' . $projects['address'] . '" class="button" target="_blank">Consulter</a>';
                        } else {
                        echo '<a class="button grey" target="_blank">Consulter</a>';
                        } ?>
                        </div>
                </div>
            </div>
        </article>
</div>
</div>


</body>
</html>

