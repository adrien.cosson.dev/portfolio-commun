<?php
if (!isset($_SESSION["username"])) {
    session_start();
}
require 'admin/database.php';

if (!empty($_GET['id'])) {
    $id = checkInput($_GET['id']);
}
function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adoptundev</title>
    <link rel="stylesheet" href="css/style.css?v=1.9">
    <link rel="stylesheet" href="css/modalLogin.css">
    <link rel="stylesheet" href="css/navbar.css">
    <link rel="stylesheet" href="css/footer.css?v=1">
<!--    <script src="js/script.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<body>
<div id="wrap">
    <header>
        <span class="close">
            <a href="#"> </a>
        </span>
        <h1><strong>adopt</strong>un<strong>dev.com</strong></h1>
        <a href="#wrap" id="open">
            <svg class="burger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
            </svg>
        </a>
        <?php
        echo '<nav>
            <div class="tab">
                <ul>';
        $db = Database::connect();

        $statement = $db->query('SELECT category FROM users');
        $users = $statement->fetch();
        echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $users['category'] . ')" id="default">Devs</a></li>';
        $statement = $db->query('SELECT resources.categoryName, resources.id AS idR, projectsCorrections.id AS idP FROM resources INNER JOIN projectsCorrections ON resources.id = projectsCorrections.resourceCategory GROUP BY resources.categoryName UNION SELECT resources.categoryName, resources.id, cours.id AS id FROM resources INNER JOIN cours ON resources.id = cours.resourceCategory GROUP BY resources.categoryName');
        $resources = $statement->fetchAll();
        if (isset($_SESSION["username"])) {
$resourcesTab=21;
            echo '<li class="hiddenMenu"><a class="tablinks" href="#" onclick="openCategory(event, ' . $resourcesTab . ')">Ressources</a>';
            echo '<ul class="underMenu">';
            foreach ($resources as $resource) {
                echo '<li><a href="#" class="tablinks" onclick="openCategory(event, ' . $resource['idR'] . ')">' . $resource['categoryName'] . '</a></li>';
            }
            echo '</ul></li>';
//        if (isset($_SESSION["username"])) {
            $statement = $db->prepare('SELECT * FROM users WHERE users.username = ?');
            $statement->execute(array($_SESSION["username"]));
            $users = $statement->fetch();

            echo '<li id="username"><a href="admin/index.php">' . $_SESSION["username"] . '</a></li>';
            echo '<li><a class="login" href="admin/logout.php">Logout</a></li>';

        } else {
            echo '<li><a href="#" id="loginButton">Login</a></li>';
        }
        echo ' </ul>
                </div>
                    <a href="#" id="close">×</a>
            </nav>
       
    </header>
</div>';

echo '<main class="container">';
//        $statement = $db->query('SELECT projectsCorrections.id, projectsCorrections.name, projectsCorrections.description, projectsCorrections.img, projectsCorrections.category, projectsCorrections.resourceCategory, projectsCorrections.address, projectsCorrections.user, resources.id AS categoryR FROM projectsCorrections INNER JOIN resources ON resources.id = projectsCorrections.resourceCategory UNION SELECT cours.id, cours.name, cours.description, cours.img, cours.category, cours.resourceCategory, cours.address, cours.user, resources.id AS categoryR FROM cours INNER JOIN resources ON resources.id = cours.resourceCategory');
        $statement = $db->query('SELECT projectsCorrections.*, categories.name as categoryName FROM projectsCorrections LEFT JOIN categories ON projectsCorrections.category = categories.id UNION SELECT cours.*, categories.name as categoryName FROM cours LEFT JOIN categories ON cours.category = categories.id ');
        echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $resourcesTab);
//foreach ($resources as $resource) {
//
//    while ($resource3 = $statement->fetch()) {
//var_dump();
//        echo '<article>
//                        <a class="article-content" href="admin/profile.php?id=' . $user['id'] . '">
//                    <div class="image">';
////        var_dump($resource3);
////        var_dump($resource3['categoryR']);
//        if (!empty($resource3['img'])) {
//            if ($resource3['resourceCategory'] == 1) {
//                echo '<img src="users/'. $resource3['user'] .'/corrections/images/'.$resource3['img'].'" alt="...">';
//            } else if ($resource3['resourceCategory'] == 2) {
//                echo '<img src="users/'.$resource3['user'].'/cours/images/'.$resource3['img'].'" alt="...">';
//            }
////            echo '<img src="images/' . $resource['img'] . '" alt="...">';
//
//        } else {
//            echo '<img src="https://picsum.photos/300/200" alt="">
//                </div>';
//
//        }
//        echo '<div>
//                                <h2>' . $resource3['name'] . '</h2>
//                                    <p>' . $resource3['description'] . '</p>';
////        if (!empty($resource3['address'])) {
////            echo '<a href="' . $resource3['address'] . '" class="button1" target="_blank">Consulter</a>';
////        } else {
////            echo '<a class="button grey" target="_blank">Consulter</a>';
////        }
//        echo '</div>
//
//                    </a>
//                </article>';
//    }
//}
//        echo '</div>';
        foreach ($resources as $resource) {

            while ($resource3 = $statement->fetch()) {
                echo '<article>';

                echo '<a class="article-content" href="#">';
                echo '<div class="image">';

                if (!empty($resource3['img'])) {
                    if ($resource3['resourceCategory'] == 1) {
                        echo '<img src="users/' . $resource3['user'] . '/corrections/images/' . $resource3['img'] . '" alt="...">';
                    } else if ($resource3['resourceCategory'] == 2) {
                        echo '<img src="users/' . $resource3['user'] . '/cours/images/' . $resource3['img'] . '" alt="...">';
                    }
                    //            echo '<img src="images/' . $resource['img'] . '" alt="...">';

                } else {
                    echo '<img src="https://picsum.photos/300/200" alt="">';
                }
                echo '</div>';
                echo '<h2>' . $resource3['name'] . '</h2>
            <p>' . $resource3['description'] . '</p>';
                //        if (!empty($resource3['address'])) {
                //            echo '<a href="' . $resource3['address'] . '" class="button1" target="_blank">Consulter</a>';
                //        } else {
                //            echo '<a class="button grey" target="_blank">Consulter</a>';
                //        }
//                echo '</div>

    echo '</a>
</article>';
            }
        }
        echo '</div>';

        foreach ($resources as $resource) {
            echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $resource['idR']);
            if ($resource["idR"] == 1) {
//        $statement = $db->query('SELECT *, resources.id AS categoryR FROM projectsCorrections INNER JOIN resources ON resources.id = projectsCorrections.resourceCategory');
                $statement = $db->query('SELECT projectsCorrections.*, categories.name as categoryName FROM projectsCorrections LEFT JOIN categories ON projectsCorrections.category = categories.id');
            } else if ($resource["idR"] == 2) {
                $statement = $db->query('SELECT *, resources.id AS categoryR FROM cours INNER JOIN resources ON resources.id = cours.resourceCategory');
            }

            $statement->execute(array($resource['idR']));
            while ($resource2 = $statement->fetch()) {
                echo '<article>';

                echo '<a class="article-content" href="#">';
                echo '<div class="image">';

                if (!empty($resource2['img'])) {
                    if ($resource2['resourceCategory'] == 1) {
                        echo '<img src="users/' . $resource2['user'] . '/corrections/images/' . $resource2['img'] . '" alt="...">';
                    } else if ($resource2['resourceCategory'] == 2) {
                        echo '<img src="users/' . $resource2['user'] . '/cours/images/' . $resource2['img'] . '" alt="...">';
                    }
                    //            echo '<img src="images/' . $resource['img'] . '" alt="...">';

                } else {
                    echo '<img src="https://picsum.photos/300/200" alt="">';
                }
                echo '</div>';
                echo '<h2>' . $resource2['name'] . '</h2>
        <p>' . $resource2['description'] . '</p>';
                //        if (!empty($resource3['address'])) {
                //            echo '<a href="' . $resource3['address'] . '" class="button1" target="_blank">Consulter</a>';
                //        } else {
                //            echo '<a class="button grey" target="_blank">Consulter</a>';
                //        }
                //                echo '</div>

                echo '</a>
</article>';
            }
            echo '</div>';
        }

//        echo '</div>';

//foreach ($resources as $resource) {
//        echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $resource['idR']);
//    if ($resource["idR"] == 1) {
////        $statement = $db->query('SELECT *, resources.id AS categoryR FROM projectsCorrections INNER JOIN resources ON resources.id = projectsCorrections.resourceCategory');
//        $statement = $db->query('SELECT projectsCorrections.*, categories.name as categoryName FROM projectsCorrections LEFT JOIN categories ON projectsCorrections.category = categories.id');
//    } else if ($resource["idR"] == 2) {
//        $statement = $db->query('SELECT *, resources.id AS categoryR FROM cours INNER JOIN resources ON resources.id = cours.resourceCategory');
//    }
//
//    $statement->execute(array($resource['idR']));
//    while ($resource2 = $statement->fetch()) {
////        var_dump($resource2);
//        echo '<article>';
//        echo '<a class="article-content" href="admin/profile.php?id=">';
//        echo '<div class="image">';
////var_dump($resource);
//        if (!empty($resource2['img'])) {
////            echo '<img src="images/' . $resource['img'] . '" alt="...">';
//            echo '<img src="users/'. $resource2['user'] .'/'.$resource["categoryName"].'/images/'. $resource2['img'].'" alt="...">';
//
//        }
//        else {
//            echo '<img src="https://picsum.photos/300/200" alt="">
//            </div>';
//
//        }
//        echo '<div>
//                                <h2>' . $resource2['name'] . '</h2>
//                                <h3>' . $resource2['categoryName'] . '</h3>
//                                    <p>' . $resource2['description'] . '</p>
//                                        <div class="article-footer">';
//        if (!empty($resource2['address'])) {
//            echo '<a href="' . $resource2['address'] . '" class="button1" target="_blank">Consulter</a>';
//        } else {
//            echo '<a class="button grey" target="_blank">Consulter</a>';
//        }
//        echo '</div>
//                            </div>
//
//                    </a>
//                </article>';
//    }
//    echo '</div>';
//
//}



echo sprintf("<div class=\"article-list tabcontent\" id=\"%s\">", $users['category']);

$statement = $db->query('SELECT * FROM users');
$users = $statement->fetchAll();

foreach ($users as $user) {
    $n = rand(100, 1000);
    echo '
                    <article>

                        <a class="article-content" href="admin/profile.php?id=' . $user['id'] . '">
                        <div class="image">';

    if (!empty($user['img'])) {
        echo '<img src="users/' . $user['id'] . '/images/'. $user['img'].'" alt="...">';
    } else {
        echo '<img src="https://picsum.photos/id/' . $n . '/300/200" alt="">';
    }
    echo '</div>
<div>
                                <h2 class="button">' . $user['username'] . '</h2>
                                    <p>' . $user['description'] . '</p>
                                        <div class="article-footer">

                                        </div>
                            </div>
                    </a>
                    </article>';
}

echo '</div>';
Database::disconnect();

?>

</main>
<!--<div id="loginModal" class="modal">-->
<!--    <div class="modal-content">-->
<!--        <input type="hidden" class="closeModal">-->
<!--        <div class="modal-header">-->
<!--            <div class="tabmodal">-->
<!--                <a class="tablinksmodal" id="defaultModal" onclick="openModal(event, 'login')">Login</a><a class="tablinksmodal" onclick="openModal(event, 'inscription')">Inscription</a>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="modal-body">-->
<!--            <div id="login" class="tabcontentmodal">-->
<!--                <!--                    <h3>London</h3>-->-->
<!--                <form id="form" class="form" method="post">-->
<!--                    <!--                    <p>Nom d'utilisateur</p>-->-->
<!--                    <label for="username">Nom d'utilisateur</label>-->
<!--                    <div class="form-group">-->
<!--                        <input type="text" class="form-control" id="username" name="username">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="password">Mot de passe</label>-->
<!--                        <input type="password" class="form-control" id="password" name="password">-->
<!--                        <span class="eye"><svg display="none" id="eye" onclick="showPassword()" aria-hidden="true"-->
<!--                                               focusable="false" data-prefix="fas" data-icon="eye"-->
<!--                                               class="svg-inline--fa fa-eye fa-w-18" role="img"-->
<!--                                               xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path-->
<!--                                        fill="currentColor"-->
<!--                                        d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path></svg>-->
<!--                            <svg display="none" id="closedEye" onclick="showPassword()" aria-hidden="true"-->
<!--                                 focusable="false" data-prefix="fas" data-icon="eye-slash"-->
<!--                                 class="svg-inline--fa fa-eye-slash fa-w-20" role="img"-->
<!--                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor"-->
<!--                                                                                                d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"></path></svg>-->
<!--                            </span>-->
<!--                        <p id="passwordLoginError">Identifiants incorrects</p>-->
<!--                    </div>-->
<!--                    <br>-->
<!--                    <div class="form-actions">-->
<!--                        <button class="loginButton" type="submit" name="login">Login</button>-->
<!--                    </div>-->
<!---->
<!--                </form>-->
<!--            </div>-->
<!--            <div id="inscription" class="tabcontentmodal">-->
<!--                <form class="form" action="admin/adduser.php" role="form" method="post" enctype="multipart/form-data">-->
<!--                    <div class="form-group">-->
<!--                        <label for="name">Nom d'utilisateur</label>-->
<!--                        <input type="text" class="form-control" id="name" name="name">-->
<!--                        <p class="help-inline">--><?php //echo $nameError; ?><!--</p>-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label for="password">Mot de passe</label>-->
<!--                        <input type="password" class="form-control" id="passwordInscription" name="password">-->
<!--                        <span class="eye"><svg display="none" id="eyeIns" onclick="showPasswordInscription()"-->
<!--                                               aria-hidden="true" focusable="false" data-prefix="fas" data-icon="eye"-->
<!--                                               class="svg-inline--fa fa-eye fa-w-18" role="img"-->
<!--                                               xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path-->
<!--                                        fill="currentColor"-->
<!--                                        d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path></svg>-->
<!--                            <svg display="none" id="closedEyeIns" onclick="showPasswordInscription()" aria-hidden="true"-->
<!--                                 focusable="false" data-prefix="fas" data-icon="eye-slash"-->
<!--                                 class="svg-inline--fa fa-eye-slash fa-w-20" role="img"-->
<!--                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor"-->
<!--                                                                                                d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"></path></svg>-->
<!--                            </span>-->
<!--                        <p class="help-inline">--><?php //echo $passwordError; ?><!--</p>-->
<!--                    </div>-->
<!--                    <br>-->
<!--                    <div class="form-actions">-->
<!--                        <button type="submit" class="loginButton">Ajouter</button>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
        <?php include ("modal.php"); ?>
        <footer>
            <a href="https://adriencosson.fr" target="_blank">2020 Adrien Cosson</a>
        </footer>
</body>
</html>
<script src="js/script.js"></script>

<script>
    // click on default tab
    // document.getElementById("default").click();
    // document.getElementById("defaultModal").click();
    // var passwordInscription = document.getElementById("passwordInscription");
    // var password = document.getElementById("password");
    // var eye = document.getElementById("eye");
    // var eyeIns = document.getElementById("eyeIns");
    // var closedEye = document.getElementById("closedEye");
    // var closedEyeIns = document.getElementById("closedEyeIns");
    // // Get the modal
    // var modal = document.getElementById("loginModal");
    //
    // // Get the button that opens the modal
    // var btn = document.getElementById("loginButton");
    //
    // // Get the <span> element that closes the modal
    // var span = document.getElementsByClassName("closeModal")[0];
    //
    // // When the user clicks on the button, open the modal
    // btn.onclick = function () {
    //     modal.style.display = "block";
    //     // document.getElementById("defaultModal").click();
    //     // tabcontent[7].style.display = "block";
    //
    // }
    //
    // // When the user clicks on <span> (x), close the modal
    // span.onclick = function () {
    //     modal.style.display = "none";
    // }
    // // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function (event) {
    //     if (event.target == modal) {
    //         modal.style.display = "none";
    //     }
    // }
    //
    // function showPasswordInscription() {
    //     if (passwordInscription.type === "password") {
    //         passwordInscription.type = "text";
    //         closedEyeIns.style.display = "block"
    //         eyeIns.style.display = "none"
    //     } else {
    //         passwordInscription.type = "password";
    //         eyeIns.style.display = "block"
    //         closedEyeIns.style.display = "none"
    //     }
    // }
    //
    // function showPassword() {
    //     if (password.type === "password") {
    //         password.type = "text";
    //         closedEye.style.display = "block"
    //         eye.style.display = "none"
    //     } else {
    //         password.type = "password";
    //         eye.style.display = "block"
    //         closedEye.style.display = "none"
    //     }
    // }


    // $(document).ready(function () {
    //     $('#form').submit(function (e) {
    //         e.preventDefault();
    //         $.ajax({
    //             type: "POST",
    //             url: 'admin/login.php',
    //             data: $(this).serialize()
    //         }).then(
    //             function (response) {
    //                 var jsonData = JSON.parse(response);
    //
    //                 if (jsonData.success === 1) {
    //                     location.href = 'index.php';
    //                 } else {
    //                     // alert('Mauvais identifiants!');
    //                     passwordLoginError = document.getElementById("passwordLoginError");
    //                     passwordLoginError.style.display = "inline-block";
    //                 }
    //             },
    //
    //             function () {
    //                 alert('I y à eu un problème!');
    //             }
    //         );
    //     });
    // });
</script>