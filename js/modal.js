// Modal
document.getElementById("defaultModal").click();
var passwordInscription = document.getElementById("passwordInscription");
var password = document.getElementById("password");
var eye = document.getElementById("eye");
var eyeIns = document.getElementById("eyeIns");
var closedEye = document.getElementById("closedEye");
var closedEyeIns = document.getElementById("closedEyeIns");
// Get the modal
var modal = document.getElementById("loginModal");

// Get the button that opens the modal
var btn = document.getElementById("loginButton");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closeModal")[0];
function openModal(evt, categoryName) {
    // Declare all variables
    var i, tabcontentmodal, tablinksmodal;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontentmodal");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinksmodal");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(categoryName).style.display = "block";
    evt.currentTarget.className += " active";
    // document.getElementById("eye").style.display = "block";
    eye.style.display = "block";
    // document.getElementById("eyeIns").style.display = "block";
    eyeIns.style.display = "block";
    // document.getElementById("eyeIns").style.display = "block";
    if (password.type === "password") {
        // passwordInscription.type = "text";
        // closedEyeIns.style.display = "none";
        closedEye.style.display = "none";
        // eyeIns.style.display = "block";
        eye.style.display = "block";
    } else {
        // password.type = "password";
        // eyeIns.style.display = "none";
        eye.style.display = "none";
        // closedEyeIns.style.display = "block";
        closedEye.style.display = "block";
    }
    if (passwordInscription.type === "password") {
        // passwordInscription.type = "text";
        closedEyeIns.style.display = "none";
        eyeIns.style.display = "block";
    } else {
        // passwordInscription.type = "password";
        eyeIns.style.display = "none";
        closedEyeIns.style.display = "block";
    }

}


// When the user clicks on the button, open the modal
btn.onclick = function () {
    modal.style.display = "block";
    eye.style.display = "block";
    // document.getElementById("defaultModal").click();
    // tabcontent[7].style.display = "block";

}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function showPasswordInscription() {
    if (passwordInscription.type === "password") {
        passwordInscription.type = "text";
        closedEyeIns.style.display = "block"
        eyeIns.style.display = "none"
    } else {
        passwordInscription.type = "password";
        eyeIns.style.display = "block"
        closedEyeIns.style.display = "none"
    }
}

function showPassword() {
    if (password.type === "password") {
        password.type = "text";
        closedEye.style.display = "block";
        eye.style.display = "none";
    } else {
        password.type = "password";
        eye.style.display = "block";
        closedEye.style.display = "none";
    }
}
$(document).ready(function () {
    $('#form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'admin/login.php',
            data: $(this).serialize()
        }).then(
            function (response) {
                var jsonData = JSON.parse(response);

                if (jsonData.success === 1) {
                    location.href = 'index.php';
                } else {
                    // alert('Mauvais identifiants!');
                    passwordLoginError = document.getElementById("passwordLoginError");
                    passwordLoginError.style.display = "inline-block";
                }
            },

            function () {
                alert('I y à eu un problème!');
            }
        );
    });
});



// ajax
// $(document).ready(function () {
//     $('#form').submit(function (e) {
// var username = document.getElementById('username').value;
// var password = document.getElementById('password').value;
// var data = {username: username, password: password};
//         console.log(data);
//         console.log(username);
//         console.log(password);
//         var xhttp = new XMLHttpRequest();
// xhttp.open("POST", "../admin/login.php", true);
// xhttp.setRequestHeader("Content-Type", "application/json");
// xhttp.onreadystatechange = function() {
//     if (this.readyState == 4 && this.status == 200) {
//
//         if (jsonData.success === 1) {
//                     location.href = 'index.php';
//                 } else {
//                     // alert('Mauvais identifiants!');
//                     passwordLoginError = document.getElementById("passwordLoginError");
//                     passwordLoginError.style.display = "inline-block";
//                 }
//     //     // Response
//     //     var response = this.responseText;
//     }
// };
// // Content-type
// xhttp.setRequestHeader("Content-Type", "application/json");
//
// // Send request with data
// xhttp.send(JSON.stringify(data));
//     });
// });